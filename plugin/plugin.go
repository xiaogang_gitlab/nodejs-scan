package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks if the filename has a .js extension
func Match(path string, info os.FileInfo) (bool, error) {
	ext := filepath.Ext(info.Name())
	if ext == ".js" {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("nodejs-scan", Match)
}

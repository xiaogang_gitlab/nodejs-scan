package main

import (
	"encoding/xml"
	"errors"
	"reflect"
	"testing"
)

func TestUnmarshal(t *testing.T) {
	json := `<rules>

<rule name="Express BodyParser Tempfile Creation Issue">
<signature>bodyParser()</signature>
<description>POST Request to Express Body Parser 'bodyParser()' can create Temporary files and consume space.</description>
<tag>module</tag>
</rule>

 <regex name="Server Side Injection(SSI) - eval()">
  <signature>(eval\()(.{0,40000})(req\.|req\.query|req\.body|req\.param)</signature>
  <description>User controlled data in eval() can result in Server Side Injection (SSI) or Remote Code Execution (RCE).</description>
  <tag>rci</tag>
 </regex>

</rules>`
	want := RuleSet{
		StringRuleDefs: []RuleDef{
			{
				Name:        "Express BodyParser Tempfile Creation Issue",
				Signature:   "bodyParser()",
				Description: `POST Request to Express Body Parser 'bodyParser()' can create Temporary files and consume space.`,
				Tag:         "module",
			},
		},
		RegexRuleDefs: []RuleDef{
			{
				Name:        "Server Side Injection(SSI) - eval()",
				Signature:   `(eval\()(.{0,40000})(req\.|req\.query|req\.body|req\.param)`,
				Description: `User controlled data in eval() can result in Server Side Injection (SSI) or Remote Code Execution (RCE).`,
				Tag:         "rci",
			},
		},
	}
	got := RuleSet{}
	if err := xml.Unmarshal([]byte(json), &got); err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestMissingIdentifier(t *testing.T) {
	set := RuleSet{
		StringRuleDefs: []RuleDef{
			{
				Name:        "Express BodyParser Tempfile Creation Issue",
				Signature:   "bodyParser()",
				Description: `POST Request to Express Body Parser 'bodyParser()' can create Temporary files and consume space.`,
				Tag:         "module",
			},
		},
		RegexRuleDefs: []RuleDef{
			{
				Name:        "Unknown rule",
				Signature:   `(eval\()(.{0,40000})(req\.|req\.query|req\.body|req\.param)`,
				Description: `User controlled data in eval() can result in Server Side Injection (SSI) or Remote Code Execution (RCE).`,
				Tag:         "rci",
			},
		},
	}

	want := errors.New("No Identifier found for: Unknown rule")
	_, got := set.Rules()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestStringRule(t *testing.T) {
	def := RuleDef{
		Name:        "Express BodyParser Tempfile Creation Issue",
		Signature:   "bodyParser()",
		Description: `POST Request to Express Body Parser 'bodyParser()' can create Temporary files and consume space.`,
		Tag:         "module",
	}
	rule := NewStringRule(def)
	sig := def.Signature

	t.Run("Match", func(t *testing.T) {
		line := "xyz bodyParser() abc"
		if !rule.Match(line) {
			t.Errorf("Expecting signature %#v to match %#v", line, sig)
		}
	})

	t.Run("No match", func(t *testing.T) {
		line := "xyz abc"
		if rule.Match(line) {
			t.Errorf("Expecting signature %v NOT TO match %v", line, sig)
		}
	})

	t.Run("Definition", func(t *testing.T) {
		got := rule.Definition()
		if !reflect.DeepEqual(def, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", def, got)
		}
	})
}

func TestRegexRule(t *testing.T) {
	def := RuleDef{
		Name:        "Server Side Injection(SSI) - eval()",
		Signature:   `eval\([^)]*req\.`,
		Description: `User controlled data in eval() can result in Server Side Injection (SSI) or Remote Code Execution (RCE).`,
		Tag:         "rci",
	}
	rule, err := NewRegexRule(def)
	if err != nil {
		t.Fatal(err)
	}
	sig := def.Signature

	t.Run("Match", func(t *testing.T) {
		line := "abc = eval(req.query) + xyz;"
		if !rule.Match(line) {
			t.Errorf("Expecting signature %#v to match %#v", line, sig)
		}
	})

	t.Run("No match", func(t *testing.T) {
		line := `abc = eval("ls") + xyz;`
		if rule.Match(line) {
			t.Errorf("Expecting signature %v NOT TO match %v", line, sig)
		}
	})

	t.Run("Definition", func(t *testing.T) {
		got := rule.Definition()
		if !reflect.DeepEqual(def, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", def, got)
		}
	})
}
